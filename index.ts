import { MySqueezeBox } from "./MySqueezeBox";

let Service: any;
let Characteristic: any;

class SqueezeBox {
    readonly name: string;
    readonly playerId: string;

    readonly api: Promise<MySqueezeBox>;

    constructor(public readonly log: any, public readonly config: any) {
        this.name = config["name"];
        this.playerId = config["playerid"];
        const email = config["email"];
        const password = config["password"];

        this.api = MySqueezeBox.login(email, password, this.playerId);
    }

    async isOn(callback: any) {

        this.log.debug("isOn");

        const api = await this.api;
        const status = await api.status();

        callback(null, status.mode === "play");
    }

    async setOn(on: boolean, callback: any) {

        this.log.debug("setOn");

        const api = await this.api;
        const powerCmd = on ? await api.play() : await api.powerOff();

        callback(null, true);
    }

    async getVolume(callback: any) {
        this.log.debug("getVolume");

        const api = await this.api;
        const volume = await api.getVolume();

        callback(null, Number(volume._volume));
    }

    async setVolume(volume: number, callback: any) {
        this.log.debug("getVolume");

        const api = await this.api;
        const volumeReply = await api.setVolume(volume);

        callback(null, true);
    }

    getServices() {
        const service = new Service.Lightbulb(this.name);
        service
            .getCharacteristic(Characteristic.On)
            .on('get', (callback: any) => this.isOn(callback).catch(err => callback(err)))
            .on('set', (on: boolean, callback: any) => this.setOn(on, callback).catch(err => callback(err)));

        service
            .getCharacteristic(Characteristic.Brightness)
            .on('get', (callback: any) => this.getVolume(callback).catch(err => callback(err)))
            .on('set', (volume: number, callback: any) => this.setVolume(volume, callback).catch(err => callback(err)));

        const info = new Service.AccessoryInformation();
        info.setCharacteristic(Characteristic.Manufacturer, "Logitech")
        info.setCharacteristic(Characteristic.Model, "Squeezebox Radio")
        info.setCharacteristic(Characteristic.SerialNumber, this.playerId);

        this.log.debug("getServices() done.");

        return [ info, service ];
    }
}

export = function(homebridge: any) {
    Service = homebridge.hap.Service;
    Characteristic = homebridge.hap.Characteristic;
    homebridge.registerAccessory("homebridge-mysqueezebox2", "MySqueezebox2", SqueezeBox);
}