import * as rp from 'request-promise-native';

export interface Status {
    seq_no: string,
    'mixer volume': number,
    player_name: string,
    playlist_tracks: number,
    player_connected: number,
    time: number,
    mode: string,
    playlist_timestamp: number,
    remote: number,
    rate: number,
    power: number,
    'playlist mode': string,
    'playlist repeat': number,
    playlist_cur_index: string,
    signalstrength: number,
    remoteMeta: any,
    'playlist shuffle': number,
    current_title: string,
    player_ip: string
}

export interface Volume {
    _volume: string
}

export class MySqueezeBox {

    private static readonly serverUrl = "https://mysqueezebox.com/user/login";
    private static readonly rpcUrl = "http://mysqueezebox.com/jsonrpc.js";

    private constructor(public readonly playerId: string, private readonly cookie: string) {
    }

    public async status(): Promise<Status> {
        return await this.exec([ "status" ]);
    }

    public async powerOff(): Promise<void> {
        return await this.exec([ "power", "0" ]);
    }

    public async play(): Promise<void> {
        return await this.exec([ "play" ]);
    }

    public async powerOn(): Promise<void> {
        return await this.exec([ "power", "1" ]);
    }

    public async getVolume(): Promise<Volume> {
        return await this.exec([ "mixer", "volume", "?" ]);
    }

    public async setVolume(volume: number): Promise<void> {
        return await this.exec([ "mixer", "volume", volume.toString() ]);
    }

    protected async exec(commands: Array<string>): Promise<any> {
        const body = {
            id: 1,
            method: "slim.request",
            params: [
                this.playerId,
                commands
            ]
        };

        const response = await rp.post({
            url: MySqueezeBox.rpcUrl,
            headers: {
                cookie: this.cookie
            },
            body,
            json: true
        });

        if (!response)
            throw new Error("Invalid response");

        if (response.error !== undefined && response.error !== null)
            throw new Error(response.error);

        console.log("RESPONSE", response);

        return response.result;
    }

    static async login(email: string, password: string, playerId: string): Promise<MySqueezeBox> {
        const form = { email, password };

        let response: any;
        try {
            response = await rp.post({ url: MySqueezeBox.serverUrl, form });
        } catch (err) {
            if (err.name !== "StatusCodeError" || err.statusCode !== 302)
                throw err;

            response = err.response;
        }

        const cookies = response.headers['set-cookie'];
        if (cookies === undefined || cookies[0] === undefined)
            throw new Error("Did not receive cookie!");

        const cookieString = cookies[0] as string;
        let cookie = cookieString.split(';')[0];

        cookie += "; Squeezebox-player=" + encodeURIComponent(playerId);
        console.log("cookie", cookie);

        return new MySqueezeBox(playerId, cookie);
    }
}
