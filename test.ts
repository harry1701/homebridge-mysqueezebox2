import { MySqueezeBox } from "./MySqueezeBox";

async function test() {
    const box = await MySqueezeBox.login("", "", "");
    const status = await box.status();
    console.log("STATUS:", status);
    //const powerOff = await box.powerOff();
    //console.log("POWEROFF", powerOff);

    const volumeReply = await box.setVolume(22);
    console.log("SETVOL", volumeReply);

    const volume = await box.getVolume();
    console.log("VOLUME", volume);

    const play = await box.play();
    console.log("PLAY", play);
}

test().catch(err => console.error("ERROR:", err));
